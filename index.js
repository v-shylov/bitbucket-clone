const path = require('path');
var clone = require('git-clone');

const teams = require('./teams.json').teams;

for (const [index, team] of teams.entries()) {
  for (const repository of team.repositories) {
    const localPath = path.join(team.owner, repository.name);
    clone(repository.url, path.join(__dirname, 'repositories', localPath), (error) => {
      if (error) {
        console.log(error);
      } else {
        console.log(localPath + ' has been successfully cloned');
      }
    })
  }
}