(async () => {
  let result = {
    teams: [],
  };
  let groups = [];
  let groupsResponce = await request('https://api.bitbucket.org/2.0/teams?role=member');
  groups = groups.concat(groupsResponce.values);
  while (groupsResponce.next) {
    groupsResponce = await request(groupsResponce.next);
    groups = groups.concat(groupsResponce.values);
  }
  
  groups = groups.filter(group => group.display_name.startsWith('viseven-test-'));

  for (const [index, group] of groups.entries()) {
    const members = await request(group.links.members.href);
    const repositories = await request(group.links.repositories.href);
    result.teams.push({
      teamname: group.username,
      owner: getTeamOwner(members.values).username,
      repositories: getTeamRepositories(repositories),
    });
    console.log(`${Math.round((index + 1) * 100 / groups.length)}%`);
  }

  console.log('DONE: ', JSON.stringify(result));
})();

function request(url) {
  return new Promise((resolve, reject) => {
    var request = new XMLHttpRequest();
    request.onload = (event) => {
      resolve(JSON.parse(event.target.response));
    };
    request.open('GET', url, true);
    request.send();
  });
};

function getTeamOwner(members) {
  return members.filter((member) => {
    return !(['anna-shavurska', 'v-shylov', 'batalinviktor', 'Svetlana_Shlyazhinskaya'].includes(member.username));
  })[0];
};

function getTeamRepositories(repositories) {
  return repositories.values.map(repository => {
    return {
      name: repository.name,
      url: `git@bitbucket.org:${repository.full_name}.git`,
    };
  });
};